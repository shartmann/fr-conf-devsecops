# fr-conf-devsecops

FR - Supports de présentation en français pour le DevSecOps

## à propos de cette présentation

L’équipe sécurité a vu émerger en seulement quelques années des pratiques de production logicielle qui bouleversent jusqu’à l’organisation et la culture de l’entreprise, au nom de la transformation numérique et du mouvement devOps.

Les mots d’ordre sont : “réduire les cycles, échouer tôt, délivrer vite, déployer en continu”. La devise : “le changement est la règle”. Les méthodes : “découpler, fractionner, abstraire, distribuer, automatiser”. Mais derrière chaque nouveau processus se cachent de mauvaises pratiques par lesquelles s’introduisent des failles de sécurité. Le RSSI peut de moins en moins référencer, vérifier, filtrer la multitude d’applications qui s’agrègent et composent le système d’information. Les mesures de sécurité obsolètes deviennent encombrantes. Contournées, elles deviennent pernicieuses.

Cette nécessaire adaptation à la transformation devOps s’appelle DevSecOps.

## support de présentation

Document unique de diaporama en PDF : [DevSecOps.pdf](./DevSecOps.pdf)

## sources et documentation

Les études et documents cités ou ayant servi à ce travail sont disponibles en PDF sur [presentations/sources](https://drive.google.com/open?id=1P9nut3-c6ZVQW_wc_Klq0owCwjO4A8zG)


## liens complémentaires

* [Annonce de la conférence du mardi 2 avril 2019](
http://www.telecom-valley.fr/2-avril-2019-communaute-securite-cloud/)
